dbConfig = {
    HOST: "mongodb://localhost",
    PORT: 27017,
    ADMINDB: "admin",
    DBADMINCOLL: "dbAdmins",
    USERADMINCOLL: "userAdmins",
    DBOWNERCOLL: "dbOwners",
    APPDB: "demoAppDB",
    USERCOLL: "Users"
};

exports.dbConfig = dbConfig;
